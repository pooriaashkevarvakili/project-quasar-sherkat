import {defineStore} from "pinia"
import axios from 'axios'
export const itemsCounter=defineStore('posts',{
    state: () => {
        return {
          posts: [],
        };
      },
    
    actions:{
        getPosts() {
            axios
              .get('https://jsonplaceholder.typicode.com/posts')
              .then((res) => {
                this.posts = res.data;
              })
              .catch((err) => {
                console.log(err);
              });
          },
            }

})

