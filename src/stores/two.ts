import { defineStore } from "pinia";

export const usetwoFirstLineLabel=defineStore('two',{
    state:()=>{
        return{ 
            twoLine:[
                {
                    id: 1,
                    name: 'Ice cream sandwich'
                  },
                  {
                    id: 2,
                    name: 237
                  },
                  {
                    id: 3,
                    name: 9
                  },
                  {
                    id: 4,
                    name: 37
                  },
                  {
                    id: 5,
                    name: 4.3
                  }
            ]
        }
    },
    getters:{},
    actions:{}
})