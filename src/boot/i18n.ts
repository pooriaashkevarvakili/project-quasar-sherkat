import { boot } from 'quasar/wrappers'
import { createI18n } from 'vue-i18n'
import messages from 'src/i18n'
import i18nData from "src/i18n/i18nData";
const i18n = createI18n({
  locale: 'fa',
  messages:i18nData

})


export default boot(({ app }) => {
  // Set i18n instance on app
  app.use(i18n)
})

export { i18n }