import { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    component: () => import('layouts/mainLayout/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Dashoboard/IndexPage.vue') },
      {
        path:'/footerSite',component:()=>import('layouts/footer/FooterSite')
      },
      {
        path:'/basketManagement',component:()=>import('layouts/sidebarDropdown/BasketManagment/BasketManagment')
      },
    {
path:'/businessPartyManagement',component:()=>import('layouts/sidebarDropdown/BusinessPartyManagement/BussinessPartyManagement')
    },
      {
        path:'/dashboard',component:()=>import('layouts/sidebarDropdown/Dashboard/Dashboard')

      },
      {
        path:'/financialManagement',component:()=>import('layouts/sidebarDropdown/FinancialManagement/FinancialManagement')
      },
      {
        path:'/productManagement',component:()=>import('layouts/sidebarDropdown/ProductManagement/ProductManagment.vue')
      },
      {
        path:'/tadbirReports',component:()=>import('layouts/sidebarDropdown/TadbirReports/TadbirReports')
      },
      {
        path:'/usersManagement',component:()=>import('layouts/sidebarDropdown/UsersManagement/UsersManagement')
      },
      {
        path:'/warehouseManagement',component:()=>import('layouts/sidebarDropdown/WarehouseManagement/WarehouseManagement')
      },
      {
        path:'/contentManagement',component:()=>import('layouts/sidebarDropdown/ContentManagement/ContentManagement')
      },
      {
        path: '/creditManagement',component:()=>import('layouts/sidebarDropdown/CreaditManagement/CreditManagement.vue')
      },
      {
        path:'/cardDashboard',component:()=>import('components/dashboard/cards/CardDashboard/CardDashboard.vue')
      },
      {
        path: '/cardDashboardOne',component:()=>import('components/dashboard/cards/CardDashboardOne/CardDashboardOne.vue')
      },
      {
        path:'/numberPagination',component:()=>import('components/dashboard/pages/NumberPagination')
      },
      {
        path: '/tableDashboard',component:()=>import('components/dashboard/tables/TableDashboard.vue')
      },
      {
        path:'/cardDashboard',component:()=>import('components/dashboard/cards/CardDashboard/CardDashboard')
      },
      {
        path:'/cardDashboardOne',component:()=>import('components/dashboard/cards/CardDashboardOne/CardDashboardOne.vue')
      },
      {
        path:'/Items',component:()=>import('pages/ProductManagement/items.vue')
      }
  
  ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/404/ErrorNotFound.vue'),
  },
];

export default routes;
