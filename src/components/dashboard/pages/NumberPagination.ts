import {ref} from 'vue'
export default {
  setup() {
    return {
      current: ref(3)
    }
  }
}
