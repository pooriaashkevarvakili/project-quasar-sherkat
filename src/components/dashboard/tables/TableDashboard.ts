import { defineComponent,reactive } from "vue"
import {useI18n} from "vue-i18n"
import {useFirstLineTabel} from "stores/first"
import {usetwoFirstLineLabel} from "stores/two"
export default{
    name:'tableDashboard',
    setup(){
           const store=useFirstLineTabel()
           const storeOne=usetwoFirstLineLabel()
            const {t} = useI18n()
             const label=reactive([
                 {
                     id:'1',
                     label:t('Name')
                 },
                 {
                     id:'2',
                     label:t('Lastname')
                 },
                 {
                     id:'3',
                     label:t('CompanyName')
                 },
                 {
                     id:4,
                     label:t('AgentType')
                 },
                 {
                     id:5,
                     label:t('Action')
                 }
             ])
          
            return{
                t,label,store,storeOne
            }
    }
}