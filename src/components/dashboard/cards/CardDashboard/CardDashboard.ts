import {defineAsyncComponent, reactive} from 'vue'
import {useI18n} from "vue-i18n";
export default {
  components:{
    CardDashboardOne:defineAsyncComponent(()=>
      import('components/dashboard/cards/CardDashboardOne/CardDashboardOne.vue')
    )
  },
  setup() {
    const {t}=useI18n()
    const cardTitle=reactive([
      {
        id:1,
        content:t('TotalReviewQueueBaskets'),
        icon:'fa fa-file-lines',
        price:42,
        background:'bg-orange-4'
      },
      {
        id:2,
        content:t('TotalProcessingBaskets'),
        price:46,
        icon: 'fa fa-cart-shopping',
        background:'bg-blue-5'
      },
      {
        id:3,
        content:t('TotalYesterdayBaskets'),
        price:0,
        icon:'fa fa-basket-shopping',
        background:'bg-pink-4'
      },
      {
        id:4,
        content:t('TotalAmountOfYesterdayBaskets'),
        price: '-',
        icon:'fa fa-dollar-sign',
        background:'bg-green-4'
      }
    ])
    return {
      t,cardTitle
    };
  },
};
