import {reactive} from "vue";
import {useI18n} from "vue-i18n";
export default {
  name: "CardSocialTwo",
  setup(){
    const {t}=useI18n()
    const cardTitle=reactive([
      {
        id:1,
        content:t('TotalTodayBaskets'),
        price:0,
        icon:'fa fa-cart-plus',
        background:'bg-purple-5'
      },
      {
        id:2,
        content: t('TotalAmountOfTodayBaskets'),
        price: 0,
        icon:'fa fa-dollar-sign',
        background:'bg-green-6'
      },
      {
        id:3,
        content: t('OnlineUsers'),
        price:0,
        icon:'people',
        background:'bg-blue-4'
      }
    ])
    return{
      t,cardTitle
    }
  }
}
