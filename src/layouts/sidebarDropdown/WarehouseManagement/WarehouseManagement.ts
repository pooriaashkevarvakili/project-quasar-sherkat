import {reactive} from "vue";
import {useI18n} from 'vue-i18n'
export default {
  name: "WarehouseManagement",
  setup(){
    const {t}=useI18n()
    const WarehouseManagement=reactive([
      {
        id:1,
        to:'/email',
        lable:t('Products'),
        icon:'fa fa-cubes'
      },
      {
        id:2,
        to:'/email',
        lable:t('Warehouses'),
        icon:'fa fa-store'
      },
      {
        id:3,
        to:'/email',
        lable:t('CheckInTransactions'),
        icon:'fa fa-receipt'
      },
      {
        id:4,
        to:'/email',
        lable:t('CheckOutTransactions'),
        icon:'edit'
      },
      {
        id:5,
        to:'/email',
        lable:t('DischargePermissions'),
        icon:'ballot'
      },
      {
        id:6,
        to:'/email',
        lable:t('TransferTransactions'),
        icon: ''
      },
      {
        id:7,
        to:'/email',
        lable:t('Cardex'),
        icon:""
      },
      {
        id:8,
        to:'/email',
        lable:t('StockBalance'),
        icon:"fa fa-warehouse"
      },
      {
        id:9,
        to:'/email',
        lable:t('InventoryTransactionOperation'),
        icon: "fa fa-user-gear"
      },
      {
        id:10,
        to:'/email',
        lable:t('InventoryReservations'),
        icon:""
      },
      {
        id:11,
        to:'/email',
        lable:t('Reports'),
        icon:"fa fa-clipboard"
      },
    ])
    return{
      WarehouseManagement,t
    }
  }
}
