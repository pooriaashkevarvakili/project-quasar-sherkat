import {reactive} from "vue";
import {useI18n} from 'vue-i18n'
export default {
  name: "creditManagement",
  setup(){
    const {t}=useI18n()
    const CreditManagement=reactive([
      {
        id:'1',
        to:'/email',
        lable:t('CreditInquiry'),
        icon:'fa fa-money-check'
      },
      {
        id:'2',
        to:'/email',
        lable:t('Transactions'),
        icon:'fa fa-money-bill'
      },
    ])
    return{
      CreditManagement,t
    }
  }
}
