import {useI18n} from "vue-i18n";
import {reactive} from "vue";

export default {
  name: "BusinessPartyManagement",
  setup(){
    const {t}=useI18n()
    const BusinessPartyManagement=reactive([
      {
        id:1,
        to:'/email',
        lable:t('Customers'),
        icon:"people"
      },
      {
        id:2,
        to:'/email',
        lable:t('Agents'),
        icon:'fa fa-user'
      },
      {
        id:3,
        to:'/email',
        lable:t('SalesPartners'),
        icon:''
      },
      {
        id:4,
        to:'/email',
        lable:t('Parties'),
        icon:''
      },
      {
        id:5,
        to:'/email',
        lable:t('CustomersSetting'),
        icon:"fa fa-gear"
      },
    ])
    return{
      BusinessPartyManagement,t
    }
  }
}
