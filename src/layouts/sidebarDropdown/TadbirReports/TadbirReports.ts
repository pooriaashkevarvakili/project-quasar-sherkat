import {useI18n} from "vue-i18n";
import {reactive} from "vue";

export default {
  name: "TadbirReports",
  setup(){
    const {t}=useI18n()
    const TadbirReports=reactive([
      {
        id:1,
        to:'/email',
        lable:t('TadbirOrders'),
      },
      {
        id:2,
        to:'/email',
        lable:t('TadbirCustomers'),
      },
      {
        id:3,
        to:'/email',
        lable:t('TadbirReceipts'),
      },
    ])
    return{
      TadbirReports,t
    }
  }
}
