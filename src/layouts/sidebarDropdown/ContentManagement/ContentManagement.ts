import {useI18n} from "vue-i18n";
import {reactive} from "vue";

export default {
  name: "ContentManagement",
  setup() {
    const {t} = useI18n()
    const ContentManagement = reactive([
      {
        id: 1,
        to: '/email',
        lable: t('Menus'),
        icon:'fa fa-bars'
      },
      {
        id: 2,
        to: '/email',
        lable: t('Banners'),
        icon:'fa fa-flag'
      },
      {
        id: 3,
        to: '/email',
        lable: t('ProductCollection'),
        icon:'fa fa-cart-plus'
      },
      {
        id: 4,
        to: '/email',
        lable: t('Pages'),
        icon:'pages'
      },
      {
        id: 5,
        to: '/email',
        lable: t('Blog'),
        icon:'fa fa-blog'
      },
      {
        id: 6,
        to: '/email',
        lable: t('ProductCommnets'),
        icon:"fa fa-basket-shopping"
      },
      {
        id: 7,
        to: '/email',
        lable: t('Forums'),
        icon:"info"
      },
      {
        id: 8,
        to: '/email',
        lable: t('FAQ'),
        icon:"fa fa-question"
      },
      {
        id: 9,
        to: '/email',
        lable: t('YaldaEvent'),
        icon:"fa fa-moon"
      },
      {
        id: 10,
        to: '/email',
        lable: t('RecruitmentRequest'),
        icon:"fas fa-address-card"
      },
      {
        id: 11,
        to: '/email',
        lable: t('WebsiteVisitors'),
        icon:"fa fa-chart-column"
      },

    ])
    return{
      ContentManagement,
      t
    }
  }
}
