import {useI18n} from 'vue-i18n'
import {reactive} from "vue";
export default {
  name: "FinancialManagement",
  setup(){
    const {t}=useI18n()
    const FinancialManagement=reactive([
      {
        id:1,
        to:'/email',
        lable:t('BankPortalPayments'),
        icon:"fa fa-landmark"
      },
    ])
    return{
      FinancialManagement,t
    }
  }
}
