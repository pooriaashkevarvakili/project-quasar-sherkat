import {useI18n} from "vue-i18n";
import {reactive} from "vue";

export default {
  name: "UsersManagement", setup(){
    const {t}=useI18n()
    const UserManagement=reactive([
      {
        id:1,
        to:'/email',
        lable:t('Users'),
        icon:'people'
      },
      {
        id:2,
        to:'/email',
        lable:t('Roles'),
        icon:"stars"
      },
      {
        id:3,
        to:'/email',
        lable:t('Permissions'),
        icon:'fa fa-users-gear'
      },
    ])
    return{
      UserManagement,t
    }
  }
}
