import {reactive} from "vue";
import {useI18n} from 'vue-i18n'
export default {
  name: "ProductManagment",
  setup(){
    const {t}=useI18n()
    const productManagement=reactive([
      {
        id:1,
        to:'/items',
        lable:t('Items'),
        icon:'fa fa-cart-plus'
      },
      {
        id:2,
        to:'/email',
        icon:'fa fa-cubes',
        lable:t('Products'),
      },
      {
        id:3,
        to:'/email',
        lable:t('Categories'),
        icon:'fa fa-building'
      },
      {
        id:4,
        to:'/email',
        lable:t('GenericProducts'),
        icon:'fa fa-briefcase'
      },
      {
        id:5,
        to:'/email',
        lable:t('Colors'),
        icon:'fa fa-palette'
      },
      {
        id:6,
        to:'/email',
        lable:t('Brands'),
        icon:'fa fa-award'
      },
      {
        id:7,
        to:'/email',
        lable:t('Warranties'),
        icon:'fa fa-certificate'
      },
      {
        id:8,
        to:'/email',
        lable:t('ConfigurableProducts'),
        icon:'fas fa-laptop'
      },
    ])
    return{
      productManagement,t
    }
  }
}
