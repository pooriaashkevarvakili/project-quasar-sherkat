import {reactive} from "vue";
import {useI18n} from 'vue-i18n'
export default {
  name: "SaleManagment",
  setup(){
    const {t}=useI18n()
    const SaleManagement=reactive([
      {
        id:1,
        to:'/email',
        lable:t('DraftOrders'),
        icon:'fa fa-file-text'
      },
      {
        id:2,
        to:'/email',
        lable:t('Orders'),
        icon:'fa fa-file-text'
      },
      {
        id:3,
        to:'/email',
        lable:t('ReturnOrders'),
        icon:'fa fa-dollar-sign'
      },
      {
        id:4,
        to:'/email',
        lable:t('Products'),
        icon:'fa fa-box-archive'
      },
      {
        id:5,
        to:'/email',
        lable:t('Pricing'),
        icon:'fa fa-tags'
      },
      {
        id:6,
        to:'/email',
        lable:t('SaleContracts'),
        icon:'fa fa-file-lines'
      },
      {
        id:7,
        to:'/email',
        lable:t('MySaleContracts'),
        icon:'fa fa-file-text'
      },
      {
        id:8,
        to:'/email',
        lable:t('Vouchers'),
        icon:'card_giftcard'
      },
      {
        id:9,
        to:'/email',
        lable:t('DiscountCodes'),
        icon:'fa fa-percentage'
      },
      {
        id:10,
        to:'/email',
        lable:t('SaleReports'),
        icon:'fa fa-file-lines'
      },
      {
        id:11,
        to:'/email',
        lable:t('SalesPersons'),
        icon:'people'
      },
      {
        id:12,
        to:'/email',
        lable:t('Quotas'),
        icon:'fa fa-building'
      },
      {
        id:13,
        to:'/email',
        lable:t('SaleProductManagers'),
        icon:'fa fa-box-archive'
      },
      {
        id:14,
        to:'/email',
        lable:t('BasicDefinitions'),
        icon:'fa fa-gears'
      },
    ])
    return{
      SaleManagement,t
    }
  }
}
