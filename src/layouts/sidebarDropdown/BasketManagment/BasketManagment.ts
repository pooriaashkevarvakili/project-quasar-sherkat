import {useI18n} from 'vue-i18n'
import {reactive} from "vue";
export default {
  name: "BasketManagment",
  setup(){
    const {t}=useI18n()
    const BasketManagement=reactive([
      {
        id:1,
        to:'/email',
        lable:t('WebSiteBaskets'),
        icon:'fa fa-cart-shopping'
      },
      {
        id:2,
        to:'/email',
        lable:t('ChannelBaskets'),
        icon:'fa fa-user'
      },
    ])
    return{
      BasketManagement,t
    }
  }
}
