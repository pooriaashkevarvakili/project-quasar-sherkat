import { useQuasar } from 'quasar';
import {reactive,onMounted} from 'vue'
import {useI18n} from "vue-i18n";
export default {

  setup(){
    const {t}=useI18n()
    const q=useQuasar()
    onMounted(() => {
      q.iconSet.arrow.dropdown = ""    
    })
    const dropdown=reactive([
      {
        id:1,
        content:t('Dashboard'),
        icon:'home'
      },
      {
        id:2,
        content:t('Profile'),
        icon:'person'
      },
      {
        id:3,
        content: t('Logout'),
        icon:'fa fa-arrow-right'
      }
    ])
    return{
      dropdown,t,q
    }
  }
}
