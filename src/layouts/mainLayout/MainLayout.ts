import {ref,defineAsyncComponent} from "vue";
export default  {
  components: {
 ProductManagement:defineAsyncComponent(()=>
 import('layouts/sidebarDropdown/ProductManagement/ProductManagment.vue')
 ),
 WarehouseManagement:defineAsyncComponent(()=>
 import('layouts/sidebarDropdown/WarehouseManagement/WarehouseManagement.vue')
 ),
 SaleManagement:defineAsyncComponent(()=>
 import('layouts/sidebarDropdown/SaleManagement/SaleManagement.vue')
 ),
 BasketManagement:defineAsyncComponent(()=>
 import('layouts/sidebarDropdown/BasketManagment/BasketManagment.vue')
 ),
 FinancialManagement:defineAsyncComponent(()=>
 import('layouts/sidebarDropdown/FinancialManagement/FinancialManagement.vue')
 ),
 BusinessPartyManagement:defineAsyncComponent(()=>
 import('layouts/sidebarDropdown/BusinessPartyManagement/BusinessPartyManagement.vue')
 ),
 CreditManagement:defineAsyncComponent(()=>
 import ('layouts/sidebarDropdown/CreaditManagement/CreditManagement.vue')
 ),
 UsersManagement:defineAsyncComponent(()=>
 import('layouts/sidebarDropdown/UsersManagement/UsersManagement.vue')
 ),
 ContentManagement:defineAsyncComponent(()=>
 import('layouts/sidebarDropdown/ContentManagement/ContentManagement.vue')
 ),
 TadbirReports:defineAsyncComponent(()=>
 import('layouts/sidebarDropdown/TadbirReports/TadbirReports.vue')
 ),
 SidebarAvajang:defineAsyncComponent(()=>
 import('layouts/sidebarLayout/SidebarAvajang/SidebarAvajang.vue')
 ),
 DropdownAvajang:defineAsyncComponent(()=>
 import('layouts/sidebarLayout/DropdownAvajang/DropdownAvajang.vue')
 ),

   
  },
  name: "MainLayout",
  setup() {
    const toggleLeftDrawerOpen = ref(false);
    return {
      toggleLeftDrawerOpen,
      toggleLeftDrawer() {
        toggleLeftDrawerOpen.value = !toggleLeftDrawerOpen.value;
      },
    };
  },
};
