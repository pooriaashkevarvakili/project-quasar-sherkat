import { defineComponent,defineAsyncComponent } from "vue"
export default defineComponent({
    name:'indexPage',
    components:{
        CardDashboard:defineAsyncComponent(()=>
        import('components/dashboard/cards/CardDashboard/CardDashboard.vue')
        ),
        TableDashboard:defineAsyncComponent(()=>
        import('components/dashboard/tables/TableDashboard.vue')
        ),
        NumberPagination:defineAsyncComponent(()=>
        import('components/dashboard/pages/NumberPagination.vue')
        ),
        FooterSite:defineAsyncComponent(()=>
import('layouts/footer/FooterSite.vue')
        )
    }
})